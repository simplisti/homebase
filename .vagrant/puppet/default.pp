#
# We install PHP
#

$packages = [
		# Required by dev tools
    'git',
		'curl',

		'php5-cli',
    'php5-fpm',

		'php5-curl',
    'php5-mysql',

		'mysql-server',
]

package { $packages:
    ensure => present,
}

service { "php5-fpm":
    ensure => running,
    require => Package['php5-fpm'],
}

file_line{ "set_date_time_fpm":
    path => "/etc/php5/fpm/php.ini",
    line => "date.timezone = 'America/Winnipeg'",
    notify => Service["php5-fpm"],
    require => Package["php5-fpm"]
}
file_line{ "set_date_time_cli":
    path => "/etc/php5/cli/php.ini",
    line => "date.timezone = 'America/Winnipeg'",
    require => Package["php5-cli"]
}

file_line{ "set_max_memory_limit":
    path => "/etc/php5/fpm/php.ini",
    line => "memory_limit = 1024M",
    notify => Service["php5-fpm"],
    require => Package["php5-fpm"]
}

file_line{ "set_max_exec_time":
    path => "/etc/php5/fpm/php.ini",
    line => "max_execution_time = 900",
    notify => Service["php5-fpm"],
    require => Package["php5-fpm"]
}

file_line{ "enable_development_errors":
    path => "/etc/php5/fpm/php.ini",
    line => "error_reporting = E_ALL",
    notify => Service["php5-fpm"],
    require => Package["php5-fpm"]
}

file_line{ "display_development_errors":
    path => "/etc/php5/fpm/php.ini",
    line => "display_errors = On",
    notify => Service["php5-fpm"],
    require => Package["php5-fpm"]
}

file_line{ "clean_uri_fix_path":
    path => "/etc/php5/fpm/php.ini",
    line => "cgi.fix_pathinfo = 1",
    notify => Service["php5-fpm"],
    require => Package["php5-fpm"]
}

file { "/etc/php5/fpm/pool.d/www.conf":
  ensure => "file",
  content => file("/var/www/.vagrant/puppet/modules/www.conf"),
  notify => Service["php5-fpm"],
  require => Package["php5-fpm"]
}

exec { "Install Composer":
    path => ["/usr/bin/"],
    command => "curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer",
    require => [Package["curl"], Package["php5-cli"]]
}

#
# We install NGINX
#

package { "nginx":
  ensure => installed
}

service { "nginx":
  require => Package["nginx"],
  ensure => running,
  enable => true
}

file { ["/etc/nginx/sites-enabled/default"]:
  require => Package["nginx"],
  ensure  => absent
}

file { "/etc/nginx/sites-available/platform":
  ensure => "file",
  content => file("/var/www/.vagrant/puppet/modules/platform"),
}

file { "/etc/nginx/sites-enabled/platform":
  require => File["/etc/nginx/sites-available/platform"],
  ensure => "link",
  notify => Service["nginx"],
  target => "/etc/nginx/sites-available/platform"
}

#
# Change NGINX user to vagrant - see the file www.conf for additional changes
# (http://symfony.com/doc/current/setup/homestead.html)v
# (http://symfony.com/doc/current/setup/file_permissions.html)
#

file_line{ "change_nginx_user_vagrant":
    path => "/etc/nginx/nginx.conf",
    line => "user vagrant;",
    match => "user www-data;",
    notify => Service["nginx"],
    require => Package["nginx"]
}

#
# https://abitwiser.wordpress.com/2011/02/24/virtualbox-hates-sendfile/
file_line { "Disable SendFile due to shared folder issues with VM":
	path => "/etc/nginx/nginx.conf",
	line => "sendfile off;",
	match => "sendfile on",
	notify => Service["nginx"],
	require => Package["nginx"]
}

#
# Configure MySQL
#

file_line{ "mysql_disable_bind_address":
    path => "/etc/mysql/my.cnf",
    line => "#bind-address",
    match => "bind-address",
    notify => Service["mysql"],
    require => Package["mysql-server"]
}

exec { "grant_privileges":
    path => ["/usr/bin/"],
    notify => Service["mysql"],
    command => "mysql -u root -e \"CREATE DATABASE IF NOT EXISTS symfony DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci; GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'; FLUSH PRIVILEGES;\"",
}

service { "mysql":
    require => Package["mysql-server"],
    ensure => running
}

#
# Install XDebug (Don't forget to change docroot path to "web" in project properties or debugging won't work as expected)
#

#package { "Install XDebug":
#    name   => 'php5-xdebug',
#    ensure => installed,
#    notify => Service["php5-fpm"],
#    require => [Package["php5-cli"], Package["php5-fpm"]]
#}
#
#file_line{ "Xdebug IDE Key":
#    path => "/etc/php5/fpm/conf.d/20-xdebug.ini",
#    line => "xdebug.idekey = netbeans-xdebug",
#    notify => Service["php5-fpm"],
#    require => Package["php5-xdebug"]
#}
#
#file_line{ "Xdebug Remote Host":
#    path => "/etc/php5/fpm/conf.d/20-xdebug.ini",
#    line => "xdebug.remote_host = 10.0.2.2",
#    notify => Service["php5-fpm"],
#    require => Package["php5-xdebug"]
#}
#
#file_line{ "Xdebug Remote Enable":
#    path => "/etc/php5/fpm/conf.d/20-xdebug.ini",
#    line => "xdebug.remote_enable = 1",
#    notify => Service["php5-fpm"],
#    require => Package["php5-xdebug"]
#}
#
#file_line{ "Xdebug Remote Autostart":
#    path => "/etc/php5/fpm/conf.d/20-xdebug.ini",
#    line => "xdebug.remote_autostart = 1",
#    notify => Service["php5-fpm"],
#    require => Package["php5-xdebug"]
#}
