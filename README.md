## Introduction

Full-stack development environment for building Symfony 3 based applications.

Building full stack applications with Symfony can require a complex development environment, installing Node, MySQL, Redis, etc locally
is often over kill and convolutes your desktop. Using VirtualBox and Vagrant we can easily (and dynamically) build the required
LAMP environment as close to production specifications as is convenient. I say convenient and not identical because I opted for 
ease of access over security with MySQL, for instance, can be accessed with user 'root' and no password.

## Requirements

1. VirtualBox
2. Vagrant

## Features

  - PHP5 
		- CLI
			- Composer 
		- FPM
 - Node
  - npm
  - less
 - NGINX
 - MySQL
 - curl
 - git

## Getting Started

1. Clone this project into your working directory
2. Call `vagrant up`
3. TODO: Show how (where?) the private key is in order to connect via Windows, NetBeans, Putty, etc
4. TODO: How do you login to the VM using 'vagrant' as a user and `sudo su` to switch to root

## Access Services

 - http://localhost:8088 
 - mysql on port 33001

## Tools

Run any of the installed commands from `/var/www`

`./vendor/bin/phploc web`

To verify the CS using FOS tool you can invoke the command directly:

`php /home/vagrant/vendor/bin/php-cs-fixer fix /var/www/project` 

TODO

 - Move SESSION storage into DB
 - Move Puppet configuration into modular shell
 - Use a configuration file similar to PuPHPet 
 - Create a configuration file to replace hard-coded values like timezone, database, etc
 - (default.pp) Delete the 'html' directory created by NGINX when installed - so annoying!

## Performance

Using Vagrant to provision your VM is awesome but it also introduces a few 
problems. Shared folders makes developing so much faster, unfortunately there 
are some issues that arise when developing, here is a list of current modifications
made in attempt to optimize the experience:

1. [Override directories](http://symfony.com/doc/current/configuration/override_dir_structure.html)

Overriding the logs, cache and vendor locations results in a pretty noticeable 
performance difference. Unfortunately this also means, you won't be able to (as easily) 
access these generated files. However, you could probably change the logger to dump to SQL
and view the errors that way. 
   
NOTE: Any changes made to *Symfony Standard Edition* required for optimization you 
should be able to review by searching "ALEX:" in the code base to find comments.