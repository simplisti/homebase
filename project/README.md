Symfony Standard Edition
========================

Some of the projects code may have been modified to work around performance issues caused
by shared folders when executing applications (specifically Symfony) using Vagrant
as a provisioning tool.