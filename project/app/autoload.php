<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/** @var ClassLoader $loader */
// ALEX: $loader = require __DIR__.'/../vendor/autoload.php';
$loader = require $_SERVER['HOME'] . '/vendor/autoload.php';

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
